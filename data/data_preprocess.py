# -*- coding: utf-8 -*-

import os, time,sys
import datetime
import numpy as np
import csv



originalfilepath = 'AOL_samples.txt'
originalfile = open(originalfilepath, "r")

processedfile = 'AOL_0.txt'
if os.path.exists(processedfile):
    os.remove(processedfile)
if not os.path.exists(processedfile):
    processedf = open(processedfile, "a")
    
processedf.write('userid' + ' ' + 'query' + ' ' + 'interaction' + ' ' + 'timestamp' + '\n')

line = originalfile.readline()
linenumber = 1
errorlines = 0

while len(line) > 0:
    try:
        context = line.split()
        if (("http:" in context[len(context)-1])|("https:" in context[len(context)-1])):
            length = len(context)
            userid = context[0]
            querylength = length - 5
            query = context[1]
            if querylength > 1:
                for x in range(2, querylength):
                    query  = query + '-' + context[x]
    
            day    = context[length-4].replace('-','')
            daytime= context[length-3].replace(':','')
            t=day+daytime
            ti=datetime.datetime.strptime(t,"%Y%m%d%H%M%S")
            timestamp=time.mktime(ti.timetuple())
            timestamp=int(timestamp)
            interaction=1
        else:
            length = len(context)
            userid = context[0]
            querylength = length - 3
            query = context[1]
            if querylength > 1:
                for x in range(2, querylength):
                    query  = query + '-' + context[x]
            day    = context[length-2].replace('-','')
            daytime= context[length-1].replace(':','')
            t=day+daytime
            ti=datetime.datetime.strptime(t,'%Y%m%d%H%M%S')
            timestamp=time.mktime(ti.timetuple())
            timestamp=int(timestamp)
            interaction=0
        processedf.write(userid + ' ' + query + ' ' + str(interaction) + ' ' + str(timestamp) + '\n')
        line = originalfile.readline()
        linenumber = linenumber + 1
    except:
        line = originalfile.readline()
        errorlines = errorlines + 1
        
print('errorlines:'+str(errorlines))

originalfile.close()
processedf.close()

print('Step1 Done!')

processedfilename = 'AOL_0.txt'
processfile0 = open(processedfilename, 'r')
Queriesline = processfile0.readline()


processfile1name='AOL_1.txt'
if os.path.exists(processfile1name):
    os.remove(processfile1name)
if not os.path.exists(processfile1name):
    processfile1 = open(processfile1name, "a")

nowlinenum = 0
while len(Queriesline) > 0:
    #print(str(linenum))
    if not (Queriesline.split()[1].__contains__('@') |
            Queriesline.split()[1].__contains__('#') |
            Queriesline.split()[1].__contains__('$') |
            Queriesline.split()[1].__contains__('%') |
            Queriesline.split()[1].__contains__('^') |
            Queriesline.split()[1].__contains__('&') |
            Queriesline.split()[1].__contains__('*') |
            Queriesline.split()[1].__contains__('(') |
            Queriesline.split()[1].__contains__(')') |
            Queriesline.split()[1].__contains__('"') |
            Queriesline.split()[1].__contains__(':') |
            Queriesline.split()[1].__contains__(';') |
            Queriesline.split()[1].__contains__(',') |
            Queriesline.split()[1].__contains__('?') |
            Queriesline.split()[1].__contains__('<') |
            Queriesline.split()[1].__contains__('>') |
            Queriesline.split()[1].__contains__('1') |
            Queriesline.split()[1].__contains__('2') |
            Queriesline.split()[1].__contains__('3') |
            Queriesline.split()[1].__contains__('4') |
            Queriesline.split()[1].__contains__('5') |
            Queriesline.split()[1].__contains__('6') |
            Queriesline.split()[1].__contains__('7') |
            Queriesline.split()[1].__contains__('8') |
            Queriesline.split()[1].__contains__('9') |
            Queriesline.split()[1].__contains__('0') |
            Queriesline.split()[1].__contains__('~') |
            Queriesline.split()[1].__contains__('_') |
            Queriesline.split()[1].__contains__('/') |
            Queriesline.split()[1].__contains__('%\%') |
            Queriesline.split()[1].startswith('-') |
            Queriesline.split()[1].endswith('-') ):
        nowlinenum = nowlinenum + 1
        processfile1.write(Queriesline)

    Queriesline = processfile0.readline()

print('nowlinenum:'+str(nowlinenum))
processfile0.close()
processfile1.close()
print('Step2 Done!')

processedfilename = 'AOL_1.txt'
processfile0= open(processedfilename, 'r')
line = processfile0.readline()
line = processfile0.readline()


csvfile=open('data_AOL.csv','wb')
writer = csv.writer(csvfile)
context=[]
while len(line)>0:
    context.append(line.split()[1])
    line = processfile0.readline()

query={}
j=0
for q in context:
    if q not in query:
        query[q]=j+3
        j=j+1
print('unique query:'+str(len(query)))

processfile0.seek(0)
line = processfile0.readline()
context1=[]
while len(line)>0:
    context1.append(line.split()[0])
    line = processfile0.readline()

user={}
ju=0
for u in context1:
    if u not in user:
        user[u]=ju+3
        ju=ju+1
print('unique user:'+str(len(user)))


processfile0.seek(0)
line = processfile0.readline()
line = processfile0.readline()
i=0
data=[]
head=['user_id','item_id','interaction_type','created_at']
data.append(head)
while(len(line)>0):
    i=i+1
    content=line.split()
    q=content[1]
    content[1]=query[q]
    data.append(content)
    line = processfile0.readline()
    

for item in data:
    writer.writerow(item)    

print('linenum:'+str(i))    
processfile0.close()
print('step 3 done')
print('All Done!')







