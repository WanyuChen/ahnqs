#!/usr/bin/env bash
cd ../src
#
# AHNQS INIT
#
#THEANO_FLAGS=mode=FAST_RUN,device=cpu 
python train_ahnqs.py lnd-test 100 100 \
--loss top1 --hidden_act tanh \
--user_propagation_mode init --user_to_output 0 \
--adapt adagrad --learning_rate 0.1 --momentum 0.0 --batch_size 2 \
--dropout_p_hidden_usr 0.0 \
--dropout_p_hidden_ses 0.1 \
--dropout_p_init 0.0 \
--n_epochs 10 --train_random_order 0 \
--eval_cutoff 10 \
--user_key user_id --item_key item_id --session_key session_id --time_key created_at \
--rnd_seed 1


